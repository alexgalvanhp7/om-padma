# posts/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('listTaller', views.TallerView.as_view()),
]