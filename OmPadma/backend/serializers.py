from rest_framework import serializers
from .models import sTaller
      
class sTallerSerializer(serializers.ModelSerializer):
  class Meta:
    model = sTaller
    fields = ('id', 'Nombre', 'Lugar', 'Horario')