from rest_framework import generics    # add this
from .serializers import sTallerSerializer      # add this
from .models import sTaller                     # add this
        

class TallerView(generics.ListAPIView):
    queryset = sTaller.objects.all()
    serializer_class = sTallerSerializer