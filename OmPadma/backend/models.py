from django.db import models
from django.contrib.auth.models import User


# Create your models here.



# class sPaciente:
#     UserID = models.ForeignKey(User, on_delete=models.CASCADE)


# class sDoctor:
#     CedulaProfesional = models.CharField(max_length=128)
#     UserID = models.ForeignKey(User, on_delete=models.CASCADE)


class sTaller(models.Model):
    Nombre = models.CharField(max_length=128)
    Lugar = models.CharField(max_length=128)
    Horario = models.CharField(max_length=128)

    class Meta:
        db_table = 'sTaller'
    
    def __str__(self):
        return self.Nombre



# class sTallerGrupo(models.Model):
#     PacienteID = models.ForeignKey(sPaciente, on_delete=models.CASCADE)
#     TallerID = models.ForeignKey(sTaller, on_delete=models.CASCADE)
